# Simple cicd template user

Contains a dummy ci file, that includes https://gitlab.com/pokexpert30/simple-template 's template. This project is used to demonstrate https://gitlab.com/gitlab-org/gitlab/-/issues/441771 by including a template using `${CI_COMMIT_REF_NAME}` and failing to resolving it in this project's gitlab pipeline editor. The pipeline still works when launched.
